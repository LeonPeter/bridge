$(document).ready(function () {
    // MATCH HEIGHT
    function matchIt() {
        $('.equalheight').matchHeight();
    };

    matchIt();


    // TAB EFFECTS
    if ($(".tabs-ul").length) {
        var tabcontent = document.getElementsByClassName("tabcontent");
        var firstClass = document.querySelector(".tablinks").getAttribute('data-class');
        $('.finprocess').hide();
        $(".tabcontent").hide();
        matchIt();

        $(".tablinks").click(function (e) {
            // e.preventDefault();
            if (!$(this).hasClass("active")) {
                $(".tablinks.active").removeClass("active");
                $(this).addClass("active");
                var dataClass = $(this).attr("data-class");
                var tabcontentLoop = document.querySelectorAll(".tabcontent");

                $(tabcontent).hide();
                for (let i = 0; i < tabcontentLoop.length; i++) {
                    const element = tabcontentLoop[i];
                    if ($(element).hasClass(dataClass)) {
                        $(element).show();

                        $(".tabcontent.active").removeClass("active");
                        $(element).addClass("active");

                        matchIt();
                        
                        $('.tabcontent.active input').change(function () {
                            $('.tabcontent.active .btn').removeClass('dis');
                        })

                        $('.tabcontent.active .btn.dis').click(function () {
                            if (document.querySelectorAll('input:checked').length < 1) {
                                alert('Please select a course you would like to study in this school');
                                $('.finprocess').hide();
                            } else {
                                $('.finprocess').slideDown();
                                $(this).removeClass('dis');
                                $('.finprocess').removeClass('d-none');
                            }
                        })
                    }

                }
            }

            var target = $(this).attr("data-class");
            var distance = $('header').outerHeight();
            $('html, body').animate({
                scrollTop: $('.' + target).offset().top - distance
            }, 1000);

        })
    }


    /*---------- SCROLL TO ----------*/
    $(".scroll-to:not(.tablinks)").click(function () {
        var target = $(this).attr("href");
        var distance = $('header').outerHeight();
        if ($(target).length) {
            $('html, body').animate({
                scrollTop: $(target).offset().top - distance
            }, 1000);
        }
        return false;
    });


    // VALIDATION
    // Wait for the DOM to be ready
    $(function () {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#bridge").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                schoolchoice: "required",
                radio: "required",
                widget_contact_form_name: "required",
                widget_contact_form_number: "required",
                widget_contact_form_date: "required",
                widget_contact_form_email: "required",
                country: "required"
            },
            // Specify validation error messages
            messages: {
                schoolchoice: "Please select a school",
                radio: "Please select a course",
                widget_contact_form_name: "Please enter your name",
                widget_contact_form_number: "Please enter your phone number",
                widget_contact_form_date: "Please enter a preferred admission date",
                country: "Please select a country"
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {
                form.submit();
            }
        });
    });


    // SELECTED SCHOOL AND COURSE
    if ($('.selectedSch').length) {
        $('input[type="radio"]').change(function () {
            var course = $(this).parent().text().trim();
            $('.selectedCourse').val('');
            $('.selectedCourse').val(course);
        })
        $('.tablinks').click(function () {
            var attr = $(this).attr('data-class');
            var title = $('.' + attr + ' .title').text()

            $('.selectedSch').val('');
            $('.selectedSch').val(title);
        })
    }
    if ($('.menulist li').length) {
        $('.menulist li').mouseenter(function() {
            let array = Array.prototype.slice.call( document.querySelectorAll('.menulist li') );
            let num = array.indexOf(this);
            let start = 0;

            do {
                document.querySelectorAll('.menulist li')[start].querySelector('a').style.color = 'white';
                document.querySelectorAll('.menulist li')[start].querySelector('a').style.opacity = '1';
                
                // if (document.querySelectorAll('.menulist li')[start].querySelector('i').length) {
                //     document.querySelectorAll('.menulist li')[start].querySelector('i').style.color = 'white';
                //     document.querySelectorAll('.menulist li')[start].querySelector('i').style.opacity = '1';
                // }
                start++;
            } while (start <= num);
        })

        $('.menulist li').mouseleave(function() {
            let array = Array.prototype.slice.call( document.querySelectorAll('.menulist li') );
            let num = array.indexOf(this);
            let start = 0;


            do {
                // alert(num)
                document.querySelectorAll('.menulist li')[start].querySelector('a').style.opacity = '.6';
                // alert(document.querySelectorAll('.menulist li')[start])
                
                // if (document.querySelectorAll('.menulist li')[start].querySelector('i').length) {
                //     document.querySelectorAll('.menulist li')[start].querySelector('i').style.opacity = '.6';
                // }
                start++;
            } while (start <= num);
            
        })
    }
})